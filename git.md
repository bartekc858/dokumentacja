# Podstawy git'a

### Dane użytkownika
- aby używać  git'a musimy skonfigurować swoją nazwę użytkownika oraz adres e-mail 
- dzięki temu każdy **commit**, który wykonamy będzie podpisany naszymi danymi
- wykonujemy to poleceniem:

	```
	git config --global user.email "<email>"
	git config --global user.name "<imie i nazwisko>"
	```

### Inicjalizacja repozytorium

- repozytorium inicjujemy poleceniem `git init` znajdując się w folderze w którym ma się znajdować repozytorium

### Dodawanie plików
- pliki dodajemy poleceniem `git add <nazwa pliku>`
- chcąc dodać wszystkie pliki utworzone w danej lokalizacji używamy `git add .`

### Pierwszy commit
- **commit** to zapisana migawka wszystkich plików naszego projektu
- przed poleceniem `git commit` musimy wywołać `git add` żeby pliki zostały dodane do śledzenia
- tworzymy go poleceniem `git commit -m "<wiadomość opisująca zmiany na plikach w projekcie>"` w momencie gdy dokonaliśmy wszystkich zmian w plikach/dodaliśmy potrzebne pliki
- wszystkie wykonane commity możemy wyświetlić poleceniem `git log`

### Łączenie ze zdalnym repozytorium

- w tym momencie musimy mieć repozytorium utworzone w takim serwisie jak **Github** lub **Bitbucket**
- aby połączyć nasze lokalne repozytorium ze zdalnym używamy polecenia 
	```
	git remote add origin <adres URL naszego repozytorium>
	git push -u origin master` oraz `git push --set-upstream origin master
	```

### Przesyłanie plików do zdalnego repozytorium

- żeby nasze lokalne pliki znalazły się w zdalnym repozytorium musimy wykonać polecenie `git push`
- teraz zostaniemy poproszeni o wpisanie hasła logowania do Bitbucket lub Github'a
- jeśli chcemy zapisać to hasło żeby nie podawać go za każdym razem wywołujemy polecenie `git config credential.helper store`
- gdy odświeżymy stronę nasze pliki będą widoczne w zdalnym repozytorium
