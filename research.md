# Kamery
- [Annke 8MP](https://pl.aliexpress.com/item/1005002611473680.html?spm=a2g0o.productlist.0.0.2f49426fZ4ndQU&algo_pvid=9365d85a-ce77-4319-8ed6-b63cb219bb18&algo_expid=9365d85a-ce77-4319-8ed6-b63cb219bb18-0&btsid=2100bddf16207453341706979e40e1&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)

# Moduły do testów
- [Przekaźnik Aqara (z N)](https://allegro.pl/oferta/aqara-pojedynczy-przekaznik-z-neutralnym-zigbee-10430156513?fromVariant=10430156566)
- [Przekaźnik Aqara (bez N)](https://allegro.pl/oferta/aqara-pojedynczy-przekaznik-bez-neutralnego-zigbee-10430156566?fromVariant=10430156513)
- [Gniazdko Aqara](https://allegro.pl/oferta/inteligentne-gniazdko-aqara-smart-plug-wersja-eu-10430156214?fromVariant=9567122306)

# Routery
- [Huawei H122](https://allegro.pl/oferta/router-huawei-h122-373-5g-lte-cpe-pro-2-wi-fi-6-10690013503?utm_feed=aa34192d-eee2-4419-9a9a-de66b9dfae24&utm_content=wybrane&utm_source=google&utm_medium=cpc&utm_campaign=_ELKTRK_PLA_Komputery&ev_adgr=Inne&gclid=CjwKCAjwv_iEBhASEiwARoemvGhYQa5MHas5Q9xHtBOdQMWiwGAdn57Jgv6kWR0cbckPXvaXDvBhGBoC-OoQAvD_BwE)