# Ściąga Vim

## Podstawy
### Vim posiada 4 tryby:

- Normalny (Normal)
- Wprowadzania tekstu (Insert / Replace)
- Wprowadzania komend (Command)
- Wizualny (Visual)

## Tryb wprowadzania tekstu
Tryb wprowadzania tekstu włączamy naciskając klawisz `i` a wyłączamy go klawiszem `Escape`.

## Tryb komend
Aby przejść do trybu wprowadzania komend naciskamy `:` w tym momencie dwukropek pojawia się na dole okna i możemy wpisać po nim komendę.
### Podstawowe polecenia to:
- `wq` - zapisz i wyjdź
- `q!` - wyjdź bez zapisywania
## Tryb normalny
### Podstawowe skróty
- `dw` - usuń całe słowo
- `x` - usuń znak po prawej stronie kursora
- `dd` - wytnij linię po prawej stronie kursora
- `P` - wklej do linii która znajduje się nad kursorem
