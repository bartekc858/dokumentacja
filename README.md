# Dokumentacja

## Git

W pliku [git.md](git.md) znajdziesz informacje o:

- tworzeniu pierwszego repozytorium git'a
- dodawaniu do niego plików
- łączeniu go ze zdalnym repozytorium.

## Vim

W pliku [vim.md](vim.md) znajdziesz ściągę skrótów niezbędnych do pracy w edytorze Vim.
